# Template básico para o Joomla 3 

Criado do zero, ou melhor, usando o bootstrap 4

Este template tem como finaidade ser fácil de entender mas usar bons recursos (error.php, component.php, entre outros), rico em posições e usar boas técnicas de criação de template. Precisa ser simples de administrar, com campos aqui no XML usados no index.php e outros bons recursos e técnicas.

## A primeira etapa deve ser a de projeto:
    - Terá as seguintes posições
        logo    search

        menu

        breadcrumbs

        slider

        novidades   populares

        left    message     right
                component
        
        footer

## Podendo ser alterado facilmente para adicionar outras.

## Lembrar que adicionei um prefixo de classe no Populares "pop" para que assuma este layout, assim como também criei uma Sobreposição para o idioma de Login para MOD_LOGIN_VALUE_USERNAME para deixar o form de login melhor, além de ter adicionado &nbsp;&nbsp; após o label Login.

- Quanto às cores ele usará 3 temas: azul escuro, verde escuro e marron

- Valorizar as regiões das posições, deixando-as espaçosas e com cores vivas as destacando

- Usar cor/imagem de fundo destaca mais que usar fundo branco. Se usar fundo branco destaque demais regiões com cores vivas.

- Sugestão: usar uma cor de fundo/imagem para toda a página e o tempalte sobre ela

- Bonitas fontes e imagens e cores agradáveis formando um tema suave em relaçao às cores e imagens

- O uso de campos no templateDetails.xml recebidos pelo index.php o torna mais flexível

- Menu drop down e fixo no topo é o torna mais amigável para o usuário

- Link no rodapé para voltar ao topo com um bom ícone apontando para o topo

- A qualidade das imagens e das fontes também é muito importante e otimizar as imagens para que não fiquem tão pesadas

- Usar boas extensões que valorizem o tempale como um bom slide show e uma bonita galeria de imagens como a Phoca Gallery

- Cuidado com as fontes, famílias e tamanhos para que se harmonizem com o template

- Criar uma boa documentação sobre o tempalte, com as posições, temas, campos, etc. Na descrição do XML inserir link para documentação

- Separar bem as coisas para evitar um index.php muito grande: style.php (css dinâmico), params.php (parâmetros do xml), fonts.php (relação de fontes) e outros

- Valorizar o rodapé, dividindo em duas ou 4 colunas e com algumas linhas de altura.

- Mais vale um template funcional e que se tenha conhecimento e controle dele do que um bonitão e complexo difícil de manter e customizar.
É importante optar por um template que possa gerenciar com conforto e não por algum que tenha muitos recursos mas seja muito complexo.

- Muito conhecimento no pequeno arquivo custom.css

## Licença

GPL 3
