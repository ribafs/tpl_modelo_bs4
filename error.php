<?php
/**
* @Package: Bootstrap
* @copyright Copyright (C) 2017 Ribamar FS. All rights reserved.
* @index.php
* 
*/

// no direct access
defined('_JEXEC') or die;

$path = JURI::base() . 'templates/'.$this->template.'/';
$doc = JFactory::getDocument();
?>

<!DOCTYPE html><html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"><head> <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<?php if (!$this->error->getCode()) : ?>
<jdoc:include type="head" />
<?php else : ?> 
<title><?php echo $this->error->getCode() ?> - <?php echo $this->title; ?></title>
<?php endif; ?>

<style>
body{
    background-image: url('<?=$path?>images/bg.png');
}
h1{
	color: #004597;
	font-size: 2.35rem;
	text-align: center;
}
.footer1, .footer2{
	background-color:#004597;
	color: #fff;
}
.footer1 img{
	display:none;
}
</style>
</head>
<body>

<?php
  if ($this->error->getCode() == '404') { ?>
    <div><h1>Desculpe! Esta página não foi encontrada.</h1></div>
    <div align="center"><img src="<?=$path?>images/404.jpg"></div>
<?php } ?>

<?php if ($this->error->getCode()) : /* check if we are on error page, if yes - display error message */ ?>
  <div align="center">
  <p><strong><?php echo JText::_('Acesse a página inicial clicando no link abaixo:'); ?></strong></p>

  <ul>
  <li><a href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></li>
  </ul>

  <p><?php echo JText::_('Caso as necessidades continuem pode entrar em contato com o administrador do site'); ?> pelo e-mail "ola@ribafs.org".</p>
<?php else : ?>
  <jdoc:include type="component" />
<?php endif; ?>
  </div>

<?php
if (JModuleHelper::getModule('footer')) { 
    echo $doc->getBuffer('module', 'footer');
}
?>

