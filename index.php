<?php
/**
* @Package: tpl_modelo_bs4
* @copyright Copyright (C) 2017 Ribamar FS. All rights reserved.
* @index.php
* 
*/

// no direct access
defined('_JEXEC') or die;

// Variáveis para ajuste do path de arquivos
$path = JURI::base() . 'templates/'.$this->template.'/';
$doc = JFactory::getDocument();

// include do params.php
require_once(JPATH_THEMES.'/'.$this->template.'/params.php');
?>

<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />	

    <!-- Inclue código importante nativo do Joomla -->
	<jdoc:include type="head" />
<?php
    // Include do CSS
	$doc->addStyleSheet('templates/' . $this->template . '/css/bootstrap.min.css');
	$doc->addStyleSheet('templates/' . $this->template . '/css/menu.css');
	$doc->addStyleSheet('templates/' . $this->template . '/css/pagination.min.css');
	$doc->addStyleSheet('templates/' . $this->template . '/css/custom.min.css');
?>	
</head>

<body>
	<div class="row" id="topo"> <!-- Linha do BS4 -->
		<div class="col-md-9"> <!-- 9 colunas da linha para o logo -->
			<h1><a href="<?=$logo_link?>"><?=$logo?></a></h1> <!-- Traz o logo e seu link do XML -->
			<b><?=$slogan?></b> <!-- Traz o slogas logo abaixo do logo-->
		</div>
		<div class="col-md-3"><!-- 3 colunas da mesma linha para a busca -->
			<jdoc:include type="modules" name="search" style="none" /> <!-- Esta linha diz ao Joomla que aqui é a posição search sem estilo-->
			</div>
		</div>

        <div class="row" id="menu">
            <?php if ($this->countModules('menu')) : ?>
            <nav class="navigation navbar navbar-expand-lg navbar-light bg-faded" role="navigation">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite" title="Menu">
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="col-md-12 collapse navbar-collapse" id="navbarSite">
                    <jdoc:include type="modules" name="menu" style="none"/>
                </div>
            </nav>
            <?php endif; ?>
        </div>

		<div class="row"><!-- Linha do BS4 -->
			<div class="col-md-12"><!-- 12 colunas para o menu. Toda a linha. -->
				<jdoc:include type="modules" name="breadcrumbs" style="none" /><!-- Esta linha diz ao Joomla que aqui é a posição breadcrumbs sem estilo-->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12"><!-- 12 colunas para o slider. Toda a linha. -->
				<jdoc:include type="modules" name="slider" style="xhtml" /><!-- Esta linha diz ao Joomla que aqui é a posição slider com estilo xhtml-->
			</div>
		</div>
		<div class="row">
			<div class="col-md-6"><!-- 6 colunas para o novidades. -->
				<jdoc:include type="modules" name="novidades" style="xhtml" /><!-- Esta linha diz ao Joomla que aqui é a posição novidades com estilo xhtml-->
			</div>
			<div class="col-md-6"><!-- as demais 6 colunas para o populares, completando a linha. -->
				<jdoc:include type="modules" name="populares" style="xhtml" /><!-- Esta linha diz ao Joomla que aqui é a posição populares com estilo xhtml-->
			</div>
		</div>

		<div class="row">
			<!-- Caso não exista módulo publicado em left mostre apenas uma coluna em branco à esquerda, o conteúdo com 8 colunas e right com 3 colunas
			Caso não exista módulo publicado em right mostre apenas uma coluna em branco à direita, o conteúdo com 8 colunas e left com 3 colunas
			Caso exista módulo publicado em left e em right então mostre o normal 3 left, 6 conteúdo e 3 right -->
			<?php if (!$this->countModules('left')){ ?>
			<div class="col-md-1"></div>
			<div class="col-md-8" id="content">
				<jdoc:include type="message"/>
				<jdoc:include type="component"/>
			</div>
			<div class="col-md-3">
				<jdoc:include type="modules" name="right" style="xhtml" />
			</div>
			<?php }elseif (!$this->countModules('right')){ ?>
			<div class="col-md-3">
				<jdoc:include type="modules" name="left" style="xhtml" />
			</div>
			<div class="col-md-8" id="content">
				<jdoc:include type="message"/>
				<jdoc:include type="component"/>
			</div>
			<div class="col-md-1"></div>
			<?php }else{?>
			<div class="col-md-3">
				<jdoc:include type="modules" name="left" style="xhtml" />
			</div>
			<div class="col-md-6" id="content">
				<jdoc:include type="message"/>
				<jdoc:include type="component"/>
			</div>
			<div class="col-md-3">
				<jdoc:include type="modules" name="right" style="xhtml" />
			</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="col-md-12">
				<jdoc:include type="modules" name="footer" style="none"/>
			</div>
		</div>
		<?php
			$doc->addScript('templates/' . $this->template . '/js/jquery.min.js');
			$doc->addScript('templates/' . $this->template . '/js/bootstrap.bundle.min.js');
		?>
	</body>
</html>
