<?php
/**
* @Package: tpl_ribafs_amd
* @copyright Copyright (C) 2017 Ribamar FS. All rights reserved.
* @index.php
* 
*/

// no direct access
defined('_JEXEC') or die;

$template = JFactory::getApplication()->getTemplate(true);
$params   = $template->params;

$logo = $params->get('logo');
$logo_link = $params->get('logo_link');
$slogan = $params->get('slogan');

